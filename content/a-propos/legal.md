+++
date = "2016-11-05T20:01:41+02:00"
title = "Mentions légales"
no_date = true
+++

## Codeurs en Liberté

* SAS coopérative au capital variable de 7400€
* 19 A Square de Monsoreau — 75020 Paris
* **SIRET**  821611431 00015
* **Responsable de publication** Tristram Gräbener
* **Contact** `bonjour@codeursenliberté.fr`

## Notre hébergeur

* OVH
* 2 rue Kellermann - 59100 Roubaix

## 🍪 Cookies et vie privée

Nous ne plaçons aucun cookie sur votre navigateur et nous
ne transmettons à des tiers aucune information vous concernant.

Nous conservons le journal d’accès au serveur http, à savoir :

* Horodotage ;
* Adresse IP ;
* Page accédée ;
* Le _User-Agent_ déclaré par votre navigateur.
